#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <assert.h>

#include "mem.h"

int main()
{
    //  initialize the allocator
    mem_init();
    mem_show_heap();
    char *p;
    
    //Test1
    p = mem_alloc(42);
    printf("allocated 42 bytes at %p\n", p);
    for (int i=0;i<5; i++){
      p = mem_alloc(42);
    }
    
    // p = mem_alloc(42) // erreur
    
    //Test2
    //p = mem_alloc(200);
    //p = mem_alloc(200); //erreur
    assert( p != NULL ); // check whether the allocation was successful
    
    mem_show_heap();
    mem_release(p);
    mem_show_heap();
    
}
