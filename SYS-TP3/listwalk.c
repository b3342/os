#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <time.h>
#include <string.h>
#include <unistd.h>

int main()
{
    int64_t N=10;

    int64_t * tab = malloc( N * sizeof(int64_t) );
    assert(tab != NULL);

    for (int64_t i=0; i<N; i++){
    	tab[i]=i;
    }

    for (int i=0; i<N; i++){
    	printf("tab[i] : %lld" PRId64, tab[i]); 
    }

    printf("tz");
    return 0;
}
